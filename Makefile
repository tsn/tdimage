# Compiler settings
EXEC_NAMES = bin/affichage bin/test bin/exemple
OBJS = obj/Image.o obj/Pixel.o

GCC = g++
FLAGS = -Wall -ggdb

LIBS = -lSDL2 -lSDL2_image -lSDL2_image
INCLUDE = -ISDL2

RM = rm -rf

# Makefile settings

########################################################################
####################### Targets beginning here #########################
########################################################################
all: $(EXEC_NAMES)

bin/affichage: $(OBJS) obj/mainAffichage.o
	$(GCC) $(FLAGS) $(OBJS) obj/mainAffichage.o -o bin/affichage $(LIBS)

bin/test: $(OBJS) obj/mainTest.o
	$(GCC) $(FLAGS) $(OBJS) obj/mainTest.o -o bin/test $(LIBS)

bin/exemple: $(OBJS) obj/mainExemple.o
	$(GCC) $(FLAGS) $(OBJS) obj/mainExemple.o -o bin/exemple $(LIBS)

obj/mainAffichage.o: src/mainAffichage.cpp src/Image.h src/Pixel.h
	$(GCC) $(FLAGS) -c src/mainAffichage.cpp -o obj/mainAffichage.o $(INCLUDE)

obj/mainExemple.o: src/mainExemple.cpp src/Image.h src/Pixel.h
	$(GCC) $(FLAGS) -c src/mainExemple.cpp -o obj/mainExemple.o $(INCLUDE)

obj/mainTest.o: src/mainTest.cpp src/Image.h src/Pixel.h
	$(GCC) $(FLAGS) -c src/mainTest.cpp -o obj/mainTest.o $(INCLUDE)

obj/Image.o: src/Image.cpp src/Image.h src/Pixel.h
	$(GCC) $(FLAGS) -c src/Image.cpp -o obj/Image.o $(INCLUDE)

obj/Pixel.o: src/Pixel.cpp src/Pixel.h
	$(GCC) $(FLAGS) -c src/Pixel.cpp -o obj/Pixel.o $(INCLUDE)

clean:
	$(RM) bin/* obj/*
