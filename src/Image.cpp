#include "Image.h"
#include <cassert>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <fstream>
using namespace std;

Image::Image() : dimx(0), dimy(0) { tab = nullptr; };

Image::Image(unsigned int dimensionX, unsigned int dimensionY) {
    assert(dimensionX > 0 && dimensionY > 0);
    dimx = dimensionX;
    dimy = dimensionY;
    tab = new Pixel[dimx * dimy];
};

Image::~Image(){
    dimx = dimy = 0;
    if (tab!=nullptr){
        delete [] tab;
        tab = nullptr;
    }
}

Pixel& Image::getPix(unsigned int x, unsigned int y) const {
    assert(x < dimx && y < dimy);
    return tab[y * dimx + x];
};

void Image::setPix(unsigned int x, unsigned int y, const Pixel& couleur){
    assert(x < dimx && y < dimy);
    Pixel &p = getPix(x, y);
    p.setBleu(couleur.getBleu());
    p.setRouge(couleur.getRouge());
    p.setVert(couleur.getVert());
}

void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax,  unsigned int Ymax, const Pixel& couleur)
{
    assert(Xmin >= 0 && Ymin >= 0);
    assert(Xmax < dimx && Ymax < dimy);
    unsigned int i, j;
    for (i = Xmin; i < Xmax; i++)
    {
        for (j = Ymin; j < Ymax; j++)
        {
            setPix(i, j, couleur);
        }
    }
}

void Image::effacer(const Pixel &couleur)
{
    dessinerRectangle(0, 0, dimx-1, dimy-1, couleur);
}

void Image::sauver(const string & filename) const {
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel& pix = getPix(x++,y);
            fichier << +pix.getRouge() << " " << +pix.getVert() << " " << +pix.getBleu() << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string & filename) {
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
	char r,g,b;
	string mot;
	dimx = dimy = 0;
	fichier >> mot >> dimx >> dimy >> mot;
	assert(dimx > 0 && dimy > 0);
	if (tab != NULL) delete [] tab;
	tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x,y).setRouge(r);
            getPix(x,y).setVert(g);
            getPix(x,y).setBleu(b);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole(){
    cout << dimx << " " << dimy << endl;
    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel& pix = getPix(x,y);
            cout << pix.getRouge() << " " << pix.getVert() << " " << pix.getBleu() << " ";
        }
        cout << endl;
    }
}

void Image::afficherInit(){
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }





    // Creation de la fenetre
    assert(m_window = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE));
    if (m_window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    assert(m_renderer = SDL_CreateRenderer(m_window,-1,SDL_RENDERER_ACCELERATED));

    assert(image = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA8888,SDL_TEXTUREACCESS_TARGET,dimx,dimy));

    //Remplir l'écran de blanc 
    SDL_SetRenderTarget(m_renderer, image);
    for (unsigned char i = 0; i < dimx; i++)
        {
            for (unsigned char j = 0; j < dimy; j++)
            {
                Pixel &tmp = getPix(i, j);
                SDL_SetRenderDrawColor(m_renderer,tmp.getRouge(),tmp.getVert(),tmp.getBleu(),SDL_ALPHA_OPAQUE);
                SDL_RenderDrawPoint(m_renderer,i,j);
                
            }
        }
    SDL_SetRenderTarget(m_renderer, nullptr);
    
    

    
  
}

void Image::actualise(float coef)
{ 
    SDL_SetRenderDrawColor(m_renderer, 226, 226, 226, 255);
    SDL_RenderClear(m_renderer);
    SDL_Rect position;
    position.x = (200 - dimx*coef)/2;
    position.y = (200 - dimy*coef)/2;
    position.w = dimx * coef;
    position.h = dimy *  coef;
    SDL_RenderCopy(m_renderer, image, nullptr, &position);
        
}

void Image::afficherBoucle(){
    cout << "boucle" << endl;
    SDL_Event events;
    bool quit = false;
    float coef = 1;

    while (!quit)
    {

        while (SDL_PollEvent(&events)){
            if (events.type == SDL_QUIT)
                quit = true;
            else if (events.type == SDL_KEYDOWN) 
            { 
				switch (events.key.keysym.scancode) 
                {
                    case SDL_SCANCODE_ESCAPE:
                    case SDL_SCANCODE_Q:
                        quit = true;
                        break;
                    case SDL_SCANCODE_T:
                        coef*=2;
                        break;
                    case SDL_SCANCODE_G:
                        coef/=2;
                        break;
                    default: break;
				}
			}
        }
        actualise(coef);
        
        SDL_RenderPresent(m_renderer);
    }
}

void Image::afficherDetruit(){
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

void Image::afficher(){
    afficherInit();
    afficherBoucle();
    afficherDetruit();
}


void Image::testRegression(){
    cout << "Début du test regression..." << endl;
    Image im1;
    Image im2(100,100);
    Pixel rouge(255,0,0);
    Pixel vert(0,255,0);

    //Constructeur par défaut
    cout << "Test du constructeur par défaut..." << endl;
    assert(im1.dimx == 0);
    assert(im1.dimy == 0);
    assert(im1.tab == NULL);
    cout << "Constructeur par défaut OK" << endl;

    //Constructeur de la classe
    cout << "Test du constructeur de la classe..." << endl;
    assert(im2.dimx == 100);
    assert(im2.dimy == 100);
    assert(im2.tab == &im2.tab[0]);
    cout << "Constructeur OK" << endl;

    //getPix
    cout << "Test de getPix..." << endl;
    assert(im2.getPix(2,1).getRouge()== im2.tab[1*im2.dimx+2].getRouge());
    assert(im2.getPix(2,1).getVert() == im2.tab[1*im2.dimx+2].getVert());
    assert(im2.getPix(2,1).getBleu() == im2.tab[1*im2.dimx+2].getBleu());
    cout << "getPix OK" << endl;

    //setPix
    cout << "Test de setPix..." << endl;
    im2.setPix(1,2,rouge);
    assert(im2.getPix(1,2).getRouge() == rouge.getRouge());
    assert(im2.getPix(1,2).getVert() == rouge.getVert());
    assert(im2.getPix(1,2).getBleu() == rouge.getBleu());
    cout << "getPix OK" << endl;

    //dessinerRectangle
    cout << "Test dessinerRectangle..." << endl;
    im2.dessinerRectangle(1,1,10,10,rouge);
    for(unsigned int j=1; j<10; ++j)
    {
        for(unsigned int i=1; i<10; ++i)
        {
            assert(im2.getPix(i,j).getRouge() == rouge.getRouge());
            assert(im2.getPix(i,j).getVert() == rouge.getVert());
            assert(im2.getPix(i,j).getBleu() == rouge.getBleu());
        }
    }
    cout << "dessinerRectangle OK" << endl;

    //Effacer
    cout << "Test de effacer..."<<endl;
    im2.effacer(vert);
    for(unsigned int j=0; j<im2.dimy; ++j)
    {
        for(unsigned int i=0; i<dimx; ++i)
        {
            assert(im2.getPix(i,j).getRouge() == vert.getRouge());
            assert(im2.getPix(i,j).getVert() == vert.getVert());
            assert(im2.getPix(i,j).getBleu() == vert.getBleu());
        }
    }
    cout << "Effacer OK" << endl;
    cout << "Fin test de regression OK." << endl;
}



