#include "Pixel.h"
#include <string>
#include <iostream>
#include <SDL2/SDL.h>
using namespace std;
#ifndef _IMAGE_H
#define _IMAGE_H


/**
 @class Classe qui génére une image, qui gère ces pixels.

*/
class Image
{
private:
    /// @param tab le tableau 2D de pixel
    Pixel * tab;

    SDL_Window *m_window;
    SDL_Renderer * m_renderer;
    SDL_Texture *image;

    /// @param dimx la dimension abscisse de l'image
    /// @param dimy la dimension ordonnée de l'image
    unsigned int dimx, dimy;

    /// @brief confingure la fenetre
    void afficherInit();

    /// @brief gère la boucle infinie de l'affichage et les évènements
    void afficherBoucle();

    /// @brief Libère al mémoire et ferme la fenetre
    void afficherDetruit();

    /// @brief gère les zooms et l'affichage de l'image
    /// @param  [in] coef Coefficient du zoom : 2 pour un zoom 0.5 pour un dézoom.
    void actualise(float);

public:
    /// @brief Constructeur par défaut de la classe: initialise dimx et dimy à 0
    Image();

    /// @brief  Constructeur de la classe: initialise dimx et dimy (après vérification) puis alloue le tableau de pixel dans le tas (image noire)
    Image(unsigned int dimensionX, unsigned int dimensionY);

    /// @brief  Destructeur de la classe: déallocation de la mémoire du tableau de pixels et mise à jour des champs dimx et dimy à 0
    ~Image();

    /** @brief Accesseur : récupère le pixel original de coordonnées (x,y) en vérifiant leur validité
     la formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x]
     @return Référence du pixel des cordonnées x,y
     */
     
    Pixel& getPix(unsigned int x, unsigned int y) const;    

    /// @brief Mutateur : modifie le pixel de coordonnées (x,y)
    void setPix(unsigned int x, unsigned int y, const Pixel& couleur);

    /// @brief Dessine un rectangle plein de la couleur dans l'image (en utilisant setPix, indices en paramètre compris)
    /// @param [in] couleur reference d'un pixel pour faire office de couleur
    /// @param [in] Xmin coin gauche du rectangle
    /// @param [in] Xmax coin droite du rectangle
    /// @param [in] Ymin coin haut
    /// @param [in] Ymax coin bas
    void dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel& couleur);

    /// @brief Efface l'image en la remplissant de la couleur en paramètre
    void effacer(const Pixel& couleur);

    /** @brief Effectue une série de tests vérifiant que le module fonctionne et
     que les données membres de l'objet sont conformes */
    void testRegression();

    /** 
      @brief convertit un fichier d'une certaine forme en une isntance d'image
      @param [in] filename nom du fichier
    */
    void ouvrir(const string &filename);

    /**
     @brief affiche dans la console l'image.
    */
    void afficherConsole();

    /**
     @brief sauvegarde en sortie l'image dans un fichier donnée en paramatre
     @param [in] filename "nom du fichier"
    */
    void sauver(const string &filename) const;

    /// @brief Affiche l'image dans une fenêtre SDL2
    void afficher();

};

#endif