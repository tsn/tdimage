Personnel : Thomas CERESA (P2207896),  BOUARROUDJ Nizar (P1805498), Salma Ahizoune hiouas (P2003590),
Source forge : https://forge.univ-lyon1.fr/tsn/tdimage

Archive pour modéliser une Image 2D en C++ avec SDL2.

Dispose de différentes fonctionnalités sur la gestion des Pixel et d'une Image : 
Création ;  
différents asseceurs/mutateurs sur un pixel d'une image par ses cordonées ;
Sauvegarde vers un fichier ppm ;
Lecture d'un fichier ppm ;
affichage d'une image dans une fenetre 100x100 ;

Aboresence classique : src/ contient le code source
			     obj/ contient les fichiers obj à compiler
			     doc/ contient ce qui concerne la documentation
			     bin/ contient les exécutable

Les executables : 
bin/exemple créer des pixels et des images. Sauvegarde une image sur un fichier. Rouvre cette image par le fichier, 
la modifie, et la resauvegarde dans un autre fichier.
bin/test fait un test de régression non exhaustif des erreur que le module Image et Pixels peuvent avoir

Pour compiler : 
les commandes make all créront ces trois executables.
make <nom_executable_choisis> marche aussi.
make clean supprime tous les fichiers dans obj/ et bin/

